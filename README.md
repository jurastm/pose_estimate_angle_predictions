### Angle predictions

It is two stage predictions system <br />
First 2D pose estimator neural net ingests image and predicts 2D skeleton. <br />
Afterwards 3D estimator neural net ingests 2D skeleton and predicts 3D skeleton <br />

# How to check angles.
Go to the [jupyter notebook](https://gitlab.com/jurastm/pose_estimate_angle_predictions/-/blob/master/angles_poc.ipynb) <br /> 
Run notebook and check quality of angles predictions from given visualizations.

