import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

pose_connection = [[0,1], [1,2], [2,3], [0,4], [4,5], [5,6], [0,7], [7,8],
                   [8,9], [9,10], [8,11], [11,12], [12,13], [8, 14], [14, 15], [15,16]]

re_order_indices= [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16]
I   = np.array([1,2,3,1,7,8,1, 13,14,15,14,18,19,14,26,27])-1 # start points
J   = np.array([2,3,4,7,8,9,13,14,15,16,18,19,20,26,27,28])-1 # end points
LR  = np.array([1,1,1,0,0,0,0, 0, 0, 0, 0, 0, 0, 1, 1, 1], dtype=bool)

def show2Dpose(vals, ax, lcolor="#3498db", rcolor="#e74c3c", add_labels=False):
    for i in np.arange( len(I) ):
        x, y = [np.array([vals[I[i], j], vals[J[i], j]]) for j in range(2)]
        ax.plot(x, y, c=pose_color[i])


def draw_skeleton(ax, skeleton, gt=False, add_index=True):
    for segment_idx in range(len(pose_connection)):
        point1_idx = pose_connection[segment_idx][0]
        point2_idx = pose_connection[segment_idx][1]
        point1 = skeleton[point1_idx]
        point2 = skeleton[point2_idx]
        color = 'k' if gt else 'r'
        plt.plot([int(point1[0]),int(point2[0])], 
                 [int(point1[1]),int(point2[1])], 
                 c=color, 
                 linewidth=2)
    if add_index:
        for (idx, re_order_idx) in enumerate(re_order_indices):
            plt.text(skeleton[re_order_idx][0], 
                     skeleton[re_order_idx][1],
                     str(idx+1), 
                     color='b'
                     )
    return

def adjust_plotting_routine(ax, vals, add_labels=True):
    RADIUS = 750 # space around the subject
    xroot, yroot, zroot = vals[0,0], vals[0,1], vals[0,2]
    ax.set_xlim3d([-RADIUS+xroot, RADIUS+xroot])
    ax.set_zlim3d([-RADIUS+zroot, RADIUS+zroot])
    ax.set_ylim3d([-RADIUS+yroot, RADIUS+yroot])
    if add_labels:
        ax.set_xlabel("x")
        ax.set_ylabel("z")
        ax.set_zlabel("y")
    ax.set_aspect('equal')
    # Get rid of the panes (actually, make them white)
    white = (1.0, 1.0, 1.0, 0.0)
    ax.w_xaxis.set_pane_color(white)
    ax.w_yaxis.set_pane_color(white)
    # Get rid of the lines in 3d
    ax.w_xaxis.line.set_color(white)
    ax.w_yaxis.line.set_color(white)
    ax.w_zaxis.line.set_color(white)
    ax.invert_zaxis()


def visualize(image, skeleton_2d, skeleton_3d):
    
    elev = 10
    azim = 70
    lcolor="#3498db"
    rcolor="#e74c3c" 
    add_labels=True
    num_joints = 16

    # Matplotlib plotting tool for visualisation.
    f = plt.figure(figsize=(15, 5))

    # Visualize original image
    ax1 = plt.subplot(1, 5, 1)
    ax1.imshow(image)
    plt.title('Input image')

    # Visualize 2d skeleton
    ax2 = plt.subplot(1, 5, 2)
    plt.title('2D key-point inputs: {:d}*2'.format(num_joints))
    ax2.set_aspect('equal')
    ax2.invert_yaxis()
    draw_skeleton(ax2, skeleton_2d, gt=True)
    plt.plot(skeleton_2d[:,0], skeleton_2d[:,1], 'ro', 2)    

    # Visualize 3D skeleton
    ax3 = plt.subplot(1, 5, 3, projection='3d')
    ax3.view_init(elev=elev, azim=azim)
    for i in np.arange(len(I) ):
        x, y, z = [np.array( [skeleton_3d[I[i], j], skeleton_3d[J[i], j]] ) for j in range(3)]
        ax3.plot(x,y, z,  lw=2, c=lcolor if LR[i] else rcolor) 
    adjust_plotting_routine(ax3, skeleton_3d)
    plt.show() 
