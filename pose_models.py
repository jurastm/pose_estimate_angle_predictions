import torch
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch.cuda.comm
from argparse import Namespace

import numpy as np
import cv2
import os
from configs.default import update_config
from configs.default import _C as cfg
from pose_hrnet import get_pose_net

def xywh2cs(x, y, w, h, aspect_ratio=0.75):
    center = np.zeros((2), dtype=np.float32)
    center[0] = x + w * 0.5
    center[1] = y + h * 0.5

    if w > aspect_ratio * h:
        h = w * 1.0 / aspect_ratio
    elif w < aspect_ratio * h:
        w = h * aspect_ratio
    scale = np.array([w * 1.0 / 200, h * 1.0 / 200], dtype=np.float32)

    return center, scale

def get_3rd_point(a, b):
    direct = a - b
    return b + np.array([-direct[1], direct[0]], dtype=np.float32)

def get_affine_transform(
        center, scale, rot, output_size,
        shift=np.array([0, 0], dtype=np.float32), inv=0
):
    if not isinstance(scale, np.ndarray) and not isinstance(scale, list):
        print(scale)
        scale = np.array([scale, scale])

    scale_tmp = scale * 200.0
    src_w = scale_tmp[0]
    dst_w = output_size[0]
    dst_h = output_size[1]

    rot_rad = np.pi * rot / 180
    src_dir = get_dir([0, src_w * -0.5], rot_rad)
    dst_dir = np.array([0, dst_w * -0.5], np.float32)

    src = np.zeros((3, 2), dtype=np.float32)
    dst = np.zeros((3, 2), dtype=np.float32)
    src[0, :] = center + scale_tmp * shift
    src[1, :] = center + src_dir + scale_tmp * shift
    dst[0, :] = [dst_w * 0.5, dst_h * 0.5]
    dst[1, :] = np.array([dst_w * 0.5, dst_h * 0.5]) + dst_dir

    src[2:, :] = get_3rd_point(src[0, :], src[1, :])
    dst[2:, :] = get_3rd_point(dst[0, :], dst[1, :])

    if inv:
        trans = cv2.getAffineTransform(np.float32(dst), np.float32(src))
    else:
        trans = cv2.getAffineTransform(np.float32(src), np.float32(dst))

    return trans

# soft-argmax
def get_max_preds_soft_pt(batch_heatmaps):
    # pytorch version of the above function using tensors
    assert len(batch_heatmaps.shape) == 4, 'batch_images should be 4-ndim'
    batch_size = batch_heatmaps.shape[0]
    num_joints = batch_heatmaps.shape[1]
    height = batch_heatmaps.shape[2]
    width = batch_heatmaps.shape[3]
    heatmaps_reshaped = batch_heatmaps.view((batch_size, num_joints, -1))
    # get score/confidence for each joint    
    maxvals = heatmaps_reshaped.max(dim=2)[0]
    maxvals = maxvals.view((batch_size, num_joints, 1))       
    heatmaps_reshaped = F.softmax(heatmaps_reshaped, dim=2)
    batch_heatmaps = heatmaps_reshaped.view(batch_size, num_joints, height, width)
    x = batch_heatmaps.sum(dim = 2)
    y = batch_heatmaps.sum(dim = 3)
    x_indices = torch.cuda.comm.broadcast(torch.arange(width).type(torch.cuda.FloatTensor), devices=[x.device.index])[0]
    x_indices = x_indices.view(1,1,width)
    y_indices = torch.cuda.comm.broadcast(torch.arange(height).type(torch.cuda.FloatTensor), devices=[x.device.index])[0]
    y_indices = y_indices.view(1,1,height)    
    x *= x_indices
    y *= y_indices
    x = x.sum(dim = 2, keepdim=True)
    y = y.sum(dim = 2, keepdim=True)
    preds = torch.cat([x, y], dim=2)
    return preds, maxvals

def get_dir(src_point, rot_rad):
    sn, cs = np.sin(rot_rad), np.cos(rot_rad)

    src_result = [0, 0]
    src_result[0] = src_point[0] * cs - src_point[1] * sn
    src_result[1] = src_point[0] * sn + src_point[1] * cs

    return src_result

class PoseEstimator:

    def __init__(self):
        
        config_file = os.path.abspath('./configs/cfgs.yaml')
        data_path = os.path.abspath('./images/')

        args = Namespace()
        args.cfg = config_file
        args.data_path = data_path
        
        update_config(cfg, args)

        cudnn.benchmark = cfg.CUDNN.BENCHMARK
        torch.backends.cudnn.deterministic = cfg.CUDNN.DETERMINISTIC
        torch.backends.cudnn.enabled = cfg.CUDNN.ENABLED

        self.model = get_pose_net(cfg, is_train=False)
        checkpoint = torch.load('./weights/final_state.pth')
        self.model.load_state_dict(checkpoint)
        self.model = torch.nn.DataParallel(self.model, device_ids=cfg.GPUS).cuda()

        normalize = transforms.Normalize( mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        self.transform = transforms.Compose([
            transforms.ToTensor(),
            normalize,
            ])

        self.image_size = (288, 384)

    def __call__(self, image):

        data_numpy = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        c, s = xywh2cs(0, 0, data_numpy.shape[1], data_numpy.shape[0])
        r = 0    
        trans = get_affine_transform(c, s, r, self.image_size)
        image_warped = cv2.warpAffine(data_numpy, trans,
                                (self.image_size[0], self.image_size[1]),
                                flags=cv2.INTER_LINEAR)
        
        inputs = self.transform(image_warped).unsqueeze(0)
        predictions_raw = self.model(inputs)
        pred, max_vals = get_max_preds_soft_pt(predictions_raw)
        pred = pred.data.cpu().numpy()

        re_order = [3, 12, 14, 16, 11, 13, 15, 1, 2, 0, 4, 5, 7, 9, 6, 8, 10]
        pred_reorder = pred[:, re_order, :]

        return pred_reorder[0], image_warped

